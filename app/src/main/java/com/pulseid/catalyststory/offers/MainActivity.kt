package com.pulseid.catalyststory.offers

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.pulseid.catalyststory.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), OffersViewHandler {
    private lateinit var pulseWebView: PulseOffersView
    private val localStorage = LocalStorage.instance

    //Store them in some secure place instead of pasting them as strings
    private var appKey: String =
        "ENTER_APP_KEY"
    private val apiSecret =
        "ENTER_APP_SECRET"
    private val euid = "ENTER_EUID"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        pulseWebView = findViewById(R.id.pulseView)
        saveCredentials()
        setupPulseWebview()
    }

    //This is for demo purpose only
    private fun saveCredentials(){
        localStorage.appKey=appKey
        localStorage.appSecret=apiSecret
        localStorage.setEuid(euid)
    }

    private fun setupPulseWebview() {
        pulseWebView.makeBackgroundTransparent()
        pulseWebView.setOffersViewListener(this)
        val pulseConfig = PulseConfig()
        pulseConfig.borderColor = "#ffffff"
        pulseWebView.initView(
            localStorage.appKey,
            localStorage.appSecret,
            localStorage.getEuid(),
            pulseConfig
        )
        pulseWebView.loadPulseUrl("ENTER_URL");
    }

    override fun onOffersLoading() {
        progressBar.visibility = View.VISIBLE
        pulseWebView.visibility = View.INVISIBLE
    }

    override fun onOffersLoaded() {
        progressBar.visibility = View.GONE
        pulseWebView.visibility = View.VISIBLE
    }

    override fun onErrorReceived() {
        //TODO add your own logic if required like may be hide pulseWebview and show your own UI
        Toast.makeText(this, "Error found", Toast.LENGTH_LONG).show()
    }

    override fun onHttpError(statusCode: Int?) {
        //TODO add your own logic if required like may be hide pulseWebview and show your own UI
        //OR Check for status code
        Toast.makeText(this, "http Error found" + statusCode.toString(), Toast.LENGTH_LONG).show()
    }

    override fun onResume() {
        super.onResume()
        pulseWebView.refresh()
    }
}