package com.pulseid.catalyststory.offers

interface OffersViewHandler {

    fun onOffersLoading()

    fun onOffersLoaded()

    fun onErrorReceived()

    fun onHttpError(statusCode: Int?)
}