package com.pulseid.catalyststory.offers

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.webkit.JavascriptInterface

class JSHandler(private val context: Context, private var listener: OnWebListener) {

    companion object {
        const val OFFER_ID = "offer_id"
    }

    @JavascriptInterface
    fun onClickOffer(id: String) {
        val intent = Intent(context, PulseOfferStoryActivity::class.java)
        intent.putExtra(OFFER_ID, id)
        context.startActivity(intent)
    }

    @JavascriptInterface
    fun onCancel() {
        if (context is Activity) {
            context.finish()
        }
    }

    @JavascriptInterface
    fun tryAgain() {
        listener.tryAgain()
    }

    public interface OnWebListener {
        fun tryAgain()
    }
}