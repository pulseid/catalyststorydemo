package com.pulseid.catalyststory.offers

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.net.Uri
import android.util.AttributeSet
import android.webkit.*
import java.util.*

@SuppressLint("SetJavaScriptEnabled", "ClickableViewAccessibility")
class PulseOffersView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null, defStyleAttr: Int = 0,
) : WebView(context, attrs, defStyleAttr), JSHandler.OnWebListener {
    companion object {
        val TERMS_LINK = "terms_link"
    }
    private var termsUrl = ""
    private var offersViewHandler: OffersViewHandler? = null
    private var headers = hashMapOf<String, String>()
    private var config = ""
    private var cachedUrl = ""

    init {
        setOnTouchListener { v, event -> // Disallow the touch request for parent scroll on touch of child view
            v.parent.requestDisallowInterceptTouchEvent(true)
            false
        }
        initWebViewClient()
        settings.useWideViewPort = true
        settings.javaScriptEnabled = true
        settings.mixedContentMode = WebSettings.MIXED_CONTENT_NEVER_ALLOW
        overScrollMode = OVER_SCROLL_NEVER;
        settings.allowFileAccess = false
        addJavascriptInterface(JSHandler(context,this), "PulseiD")
    }

    fun initView(appKey: String?, appSecret: String?, userId: String?) {
        headers["x-api-key"] = appKey!!
        headers["x-api-secret"] = appSecret!!
        headers["euid"] = userId!!
    }

    fun initView(appKey: String?, appSecret: String?, userId: String?, config: PulseConfig) {
        headers["x-api-key"] = appKey!!
        headers["x-api-secret"] = appSecret!!
        headers["euid"] = userId!!
        termsUrl = config.generalTermsAndConditionsUrl.toString()
        this.config = Config(config).getConfigString()
    }

    private fun initWebViewClient() {
        webViewClient = object : WebViewClient() {
            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                offersViewHandler?.onOffersLoading()
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                if (config.isEmpty()) {
                    //Change with other library if not using Gson.
                    updateConfig(config)
                } else {
                    updateConfig(config)
                }
                offersViewHandler?.onOffersLoaded()
            }

            override fun onReceivedError(
                view: WebView?,
                request: WebResourceRequest?,
                error: WebResourceError?
            ) {
                super.onReceivedError(view, request, error)
                if (cachedUrl.contains("story.html")) {
                    loadUrl("file:///android_asset/error_story.html")
                } else {
                    loadUrl("file:///android_asset/error_carousel.html")
                }
                offersViewHandler?.onErrorReceived()
            }

            override fun onReceivedHttpError(
                view: WebView?,
                request: WebResourceRequest?,
                errorResponse: WebResourceResponse?
            ) {
                super.onReceivedHttpError(view, request, errorResponse)
                offersViewHandler?.onHttpError(errorResponse?.statusCode)
            }

            override fun shouldOverrideUrlLoading(
                view: WebView?,
                request: WebResourceRequest?
            ): Boolean {
                if (isAcceptedUrl(request!!.url)) {
                    val intent = Intent(context, TermsOfServiceActivity::class.java)
                    intent.putExtra(TERMS_LINK, termsUrl)
                    context.startActivity(intent)
                } else {
                    offersViewHandler?.onErrorReceived()
                }
                return true
            }
        }
    }

    fun setOffersViewListener(offersViewHandler: OffersViewHandler) {
        this.offersViewHandler = offersViewHandler
    }

    private fun isAcceptedUrl(requestUrl: Uri?): Boolean {
        if (requestUrl != null && requestUrl.scheme != null) {
            if (requestUrl.scheme == "https") {
                return requestUrl.toString().contains(termsUrl)
            }
        }
        return false
    }

    /**
     * It will make the webview background transparent
     */
    fun makeBackgroundTransparent() {
        setBackgroundColor(Color.TRANSPARENT)
    }

    /**
     * @param headers is map of <String, String> to send app keys for authorization,
     * It will be injected from the app
     */
    fun addHeaders(headers: HashMap<String, String>) {
        this.headers = headers
    }

    /**
     * @param url is the pulse offers url injected from the app.
     */
    fun loadPulseUrl(url: String) {
        cachedUrl = url
        //WARNING:DO NOT REMOVE THIS AS THIS WILL BE USED TO TRACE ISSUE FOR DEBUGGING.
        headers.put("trace-id", UUID.randomUUID().toString())
        loadUrl(url, headers)
    }

    /**
     * @param json It is the json string of PulseConfig to change the UI of website like background color, border color.
     */
    private fun updateConfig(json: String) {
        evaluateJavascript("renderApp(" + json + ");", null)
    }

    override fun tryAgain() {
        post{loadPulseUrl(cachedUrl)}
    }

    fun refresh() {
        evaluateJavascript("refetchOfferAttribution();", null)
    }
}