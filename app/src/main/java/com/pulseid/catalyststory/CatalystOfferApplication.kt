package com.pulseid.catalyststory

import android.app.Application

class CatalystStoryApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    companion object {
        lateinit var instance: CatalystStoryApplication
            private set
    }
}