package com.pulseid.catalyststory.offers

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.pulseid.catalyststory.R

class TermsOfServiceActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_terms)
        // assigning ID of the toolbar to a variable
        val toolbar = findViewById<Toolbar>(R.id.toolbar)

        // using toolbar as ActionBar
        setSupportActionBar(toolbar)
        if (supportActionBar != null) {
            supportActionBar!!.setHomeButtonEnabled(true)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }
        val webView = findViewById<WebView>(R.id.webview)
        setupWebview(webView)
    }

    private fun setupWebview(webView: WebView) {
        val url = intent.getStringExtra(PulseOffersView.TERMS_LINK)
        if (supportActionBar != null) {
            supportActionBar!!.title = title
        }
        webView.webViewClient = initClient()
        val webSettings = webView.settings
        webSettings.allowFileAccess = false
        webView.overScrollMode = View.OVER_SCROLL_NEVER
        webView.loadUrl(url!!)
    }

    private fun initClient(): WebViewClient {
        return object : WebViewClient() {}
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return true
    }
}

