package com.pulseid.catalyststory.offers

import com.google.gson.Gson

data class PulseConfig(
    var generalTermsAndConditionsLinkText: String? = null,
    var generalTermsAndConditionsUrl: String? = null,
    var backgroundColor: String? = null,
    var borderColor: String? = null,
    var title: String? = null,
    var subTitle: String? = null,
    var carouselPaddingLeft: Int = 12,//default value 12 in pixels
    var story: StoryConfig?=null,
    var offersPerPage: Int?=null,
)

data class Config(val config: PulseConfig) {
    fun getConfigString(): String {
        return Gson().toJson(Config(config))
    }
}

data class StoryConfig(
    var duration: Long? = null,
    var ctaText: String? = null,
    var activatedCtaText: String? = null,
    var successPopupHeading: String? = null,
    var successPopupMessage: String? = null,
)