package com.pulseid.catalyststory.offers

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.pulseid.catalyststory.R
import com.pulseid.catalyststory.offers.JSHandler.Companion.OFFER_ID
import kotlinx.android.synthetic.main.activity_pulse_offers_story.*

class PulseOfferStoryActivity : AppCompatActivity(), OffersViewHandler {
    private lateinit var pulseWebView: PulseOffersView
    private val localStorage = LocalStorage.instance
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pulse_offers_story)
        pulseWebView = findViewById(R.id.pulseView)
        setupPulseWebview()
    }

    private fun setupPulseWebview() {
        val data = intent.getStringExtra(OFFER_ID)
        pulseWebView.setOffersViewListener(this)
        val pulseConfig = PulseConfig()
        pulseConfig.generalTermsAndConditionsLinkText = "View terms and condition"
        pulseConfig.generalTermsAndConditionsUrl = "https://www.google.com"
        pulseWebView.initView(
            localStorage.appKey, localStorage.appSecret, localStorage.getEuid(), pulseConfig
        )
        data.let {
            pulseWebView.loadPulseUrl("ENTER_URL$it")
        }
    }

    override fun onOffersLoading() {
        progressBar.visibility = View.VISIBLE
        pulseWebView.visibility = View.INVISIBLE
    }

    override fun onOffersLoaded() {
        progressBar.visibility = View.INVISIBLE
        pulseWebView.visibility = View.VISIBLE
    }

    override fun onErrorReceived() {
    }

    override fun onHttpError(statusCode: Int?) {
    }
}