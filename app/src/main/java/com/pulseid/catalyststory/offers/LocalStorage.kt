package com.pulseid.catalyststory.offers

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import com.pulseid.catalyststory.CatalystStoryApplication

internal class LocalStorage private constructor() {
    private val preferences: SharedPreferences =
        CatalystStoryApplication.instance.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
    private val r2d2: R2d2 = R2d2(CatalystStoryApplication.instance.applicationContext, KEY_ALIAS)
    var appKey: String
        get() {
            var apiKey = preferences.getString(KEY_API, null)
            val decrypted = r2d2.decryptData(apiKey!!)
            if (decrypted != null && !decrypted.equals("", ignoreCase = true)) {
                apiKey = decrypted
            }
            return apiKey
        }
        set(appKey) {
            var apiKey = appKey
            val encrypted = r2d2.encryptData(apiKey!!)
            if (encrypted != null && !encrypted.equals("", ignoreCase = true)) {
                apiKey = encrypted
                Log.d("apikeyEncrpt", apiKey!!)
            }
            val editor = preferences.edit()
            editor.putString(KEY_API, apiKey)
            editor.apply()
        }
    var appSecret: String?
        get() {
            var appSecret = preferences.getString(KEY_APP_SECRET, null)
            val decrypted = r2d2.decryptData(appSecret!!)
            if (decrypted != null && !decrypted.equals("", ignoreCase = true)) {
                appSecret = decrypted
            }
            return appSecret
        }
        set(appSecret) {
            var appSecret = appSecret
            val encrypted = r2d2.encryptData(appSecret!!)
            if (encrypted != null && !encrypted.equals("", ignoreCase = true)) {
                appSecret = encrypted
                Log.d("appSecretEncrpt", appSecret!!)
            }
            val editor = preferences.edit()
            editor.putString(KEY_APP_SECRET, appSecret)
            editor.apply()
        }

    companion object {
        private const val PREFS_NAME = "com.catalyst.pulseid.SharedPrefs"
        private const val KEY_ALIAS = "key_alias"
        private const val KEY_API = "key_api"
        private const val KEY_APP_SECRET = "key_app_secret"
        private const val EUID = "EUID"

        val instance = LocalStorage()
    }

    fun setEuid(euid: String?) {
        val editor = preferences.edit()
        editor.putString(EUID, euid)
        editor.apply()
    }

    fun getEuid(): String? {
        return preferences.getString(EUID, "")
    }

}